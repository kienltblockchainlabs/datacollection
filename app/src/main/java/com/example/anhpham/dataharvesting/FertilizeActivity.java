package com.example.anhpham.dataharvesting;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.widget.Button;
import android.view.View;
import android.view.Window;
import android.view.KeyEvent;
import android.view.View.OnClickListener;
import android.content.Intent;
import android.app.Dialog;
import android.content.DialogInterface;
import android.widget.TextView;


public class FertilizeActivity extends Activity {
    MediaPlayer song_fertilizer1;
    MediaPlayer song_organic;
    MediaPlayer song_npk;
    MediaPlayer song_hai;
    MediaPlayer song_other_fertilizer;
    MediaPlayer song_cancel;
    MediaPlayer song_success;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        song_fertilizer1 = MediaPlayer.create(FertilizeActivity.this, R.raw.fertilizer);
        song_organic = MediaPlayer.create(FertilizeActivity.this, R.raw.organic_confirm);
        song_npk = MediaPlayer.create(FertilizeActivity.this, R.raw.fertilize_npk);
        song_hai = MediaPlayer.create(FertilizeActivity.this, R.raw.fertilize_hai);
        song_other_fertilizer = MediaPlayer.create(FertilizeActivity.this, R.raw.organic_confirm);
        song_cancel = MediaPlayer.create(FertilizeActivity.this, R.raw.cancel);
        song_success = MediaPlayer.create(FertilizeActivity.this, R.raw.success);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fertilize);
        Button mReturnBtn = (Button) findViewById(R.id.return_selection_button);

        song_fertilizer1.start();
        mReturnBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                goSelectionActivity(v);
            }
        });

        Button mOrganicBtn = (Button) findViewById(R.id.organic_fertilizer_button);
        mOrganicBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                showQuestionDialog("Bạn Xác Nhận Vừa Thực Hiện Bón Phân Hữu Cơ?","11");
                song_fertilizer1.stop();
                song_organic.start();
            }
        });

        Button mNPKBtn = (Button) findViewById(R.id.npk_fertilizer_button);
        mNPKBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                showQuestionDialog("Bạn Xác Nhận Vừa Thực Hiện Bón Phân NPK?", "12");
                song_fertilizer1.stop();
                song_npk.start();
            }
        });

        Button mHAIBtn = (Button) findViewById(R.id.hai_fertilizer_button);
        mHAIBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                showQuestionDialog("Bạn Xác Nhận Vừa Thực Hiện Bón Phân HAI?", "13");
                song_fertilizer1.stop();
                song_hai.start();
            }
        });

        Button mOtherBtn = (Button) findViewById(R.id.others_fertilizer_button);
        mOtherBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                showQuestionDialog("Đọc Tên Loại Phân Bón Bạn Vừa Bón?", "otherFertilizers");
                song_fertilizer1.stop();
                song_other_fertilizer.start();
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    private void goSelectionActivity (View v) {
        Intent intent = new Intent (this, SelectionActivity.class);
        startActivity (intent);
        overridePendingTransition(R.anim.anim_12, R.anim.anim_23);
    }

    private void showQuestionDialog (String message, final String activity) {
        final Dialog myDialog = new Dialog(this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.confirm_dialog_layout);
        myDialog.setCancelable(false);

        TextView mTextMessage  = (TextView) myDialog.findViewById(R.id.dialog_message);
        mTextMessage.setText(message);

        Button mYesBtn = (Button) myDialog.findViewById(R.id.yes_button);
        Button mNoBtn = (Button) myDialog.findViewById(R.id.no_button);
        mNoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Do your code here
                song_success.stop();
                song_cancel.stop();
                song_npk.stop();
                song_hai.stop();
                song_organic.stop();
                song_other_fertilizer.stop();
                song_cancel.start();
                myDialog.dismiss();
            }
        });

        mYesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Do your code here

                new HttpGetRequest().execute("https://glacial-temple-77441.herokuapp.com/activity?time=32&activity=" + activity);
                song_success.stop();
                song_cancel.stop();
                song_npk.stop();
                song_hai.stop();
                song_organic.stop();
                song_other_fertilizer.stop();
                song_success.start();
                myDialog.dismiss();
            }
        });

        myDialog.show();
        myDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.cancel();
                    return true;
                }
                return false;
            }
        });
    }
}
