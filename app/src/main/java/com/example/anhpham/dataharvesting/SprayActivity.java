package com.example.anhpham.dataharvesting;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.widget.Button;
import android.view.*;
import android.view.Window;
import android.view.KeyEvent;
import android.view.View.OnClickListener;
import android.content.Intent;
import android.app.Dialog;
import android.content.DialogInterface;
import android.widget.TextView;


public class SprayActivity extends Activity {
    MediaPlayer song_spray;
    MediaPlayer song_antracol;
    MediaPlayer song_cyrux;
    MediaPlayer song_karate;
    MediaPlayer song_others;
    MediaPlayer song_success;
    MediaPlayer song_cancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        song_spray = MediaPlayer.create(this,R.raw.spray);
        song_antracol = MediaPlayer.create(this,R.raw.spray_antracol);
        song_cyrux = MediaPlayer.create(this,R.raw.spray_cyrux);
        song_karate = MediaPlayer.create(this,R.raw.spray_karate);
        song_others = MediaPlayer.create(this,R.raw.spray_others);
        song_success = MediaPlayer.create(this,R.raw.success);
        song_cancel = MediaPlayer.create(this,R.raw.cancel);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spray);
        song_spray.start();
        Button mReturnBtn = (Button) findViewById(R.id.return_selection_button);
        mReturnBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                goSelectionActivity(v);
            }
        });

        Button mAntracolBtn = (Button) findViewById(R.id.antralcol_pesticide_button);
        mAntracolBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                showQuestionDialog("Bạn Xác Nhận Vừa Thực Hiện Phun Thuốc Antracol ?", "31");
                song_spray.stop();
                song_antracol.start();
            }
        });

        Button mKarateBtn = (Button) findViewById(R.id.karate_pesticide_button);
        mKarateBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                showQuestionDialog("Bạn Xác Nhận Vừa Thực Hiện Phun Thuốc Karate?", "32");
                song_spray.stop();
                song_karate.start();
            }
        });

        Button mCyruxBtn = (Button) findViewById(R.id.cyrux_pesticide_button);
        mCyruxBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                showQuestionDialog("Bạn Xác Nhận Vừa Thực Hiện Phun Thuốc Cyrux?", "32");
                song_spray.stop();
                song_cyrux.start();
            }
        });

        Button mOthersBtn = (Button) findViewById(R.id.other_pesticides_button);
        mOthersBtn.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                showQuestionDialog("Đọc Tên Loại Thuốc Bạn Vừa Phun", "otherPesticides");
                song_spray.stop();
                song_others.start();
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    private void goSelectionActivity (View v) {
        Intent intent = new Intent (this, SelectionActivity.class);
        startActivity (intent);
        overridePendingTransition(R.anim.anim_12, R.anim.anim_23);
        song_spray.stop();
        song_antracol.stop();
        song_cyrux.stop();
        song_karate.stop();
        song_others.stop();
        song_success.stop();
        song_cancel.stop();
    }

    private void showQuestionDialog (String message, final String activity) {
        final Dialog myDialog = new Dialog(this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.confirm_dialog_layout);
        myDialog.setCancelable(false);

        TextView mTextMessage  = (TextView) myDialog.findViewById(R.id.dialog_message);
        mTextMessage.setText(message);

        Button mYesBtn = (Button) myDialog.findViewById(R.id.yes_button);
        Button mNoBtn = (Button) myDialog.findViewById(R.id.no_button);
        mNoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Do your code here
                song_success.stop();
                song_cancel.stop();
                song_spray.stop();
                song_antracol.stop();
                song_cyrux.stop();
                song_karate.stop();
                song_others.stop();
                song_success.stop();
                song_cancel.start();
                myDialog.dismiss();
            }
        });

        mYesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Do your code here
                new HttpGetRequest().execute("https://glacial-temple-77441.herokuapp.com/activity?time=32&activity=" + activity);
                song_success.stop();
                song_cancel.stop();
                song_spray.stop();
                song_antracol.stop();
                song_cyrux.stop();
                song_karate.stop();
                song_others.stop();
                song_cancel.stop();
                song_success.start();
                myDialog.dismiss();
            }
        });

        myDialog.show();
        myDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.cancel();
                    return true;
                }
                return false;
            }
        });
    }
}